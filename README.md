# DTN's Micro Front End Spike

## Getting Started

### Setting Up the Development Environment

1. Clone the project
2. `cd` in to the repository `cd eco-portal-micro-front-end-spike`

## Start app-one project

1. `cd` in to the repository `cd app-one`
2. Run `npm run install` to install the npm dependencies
3. Run `npm run start` to start the app locally

## Start host project

1. `cd` in to the repository `cd host`
2. Run `npm run install` to install the npm dependencies
3. Run `npm run start` to start the app locally

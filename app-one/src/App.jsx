import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import List from "./components/List";
import Test from "./components/Test";

import "./index.css";

const App = ({ user }) => {
  return (
    <Router>
      <div className="container my-5">
        <h1>APP ONE</h1>
        {user && <h2>USER: {user}</h2>}
      </div>
      <Switch>
        <Route exact path="/" component={List} />
        <Route exact path="/test" component={Test} />
      </Switch>
    </Router>
  );
};

export default App;

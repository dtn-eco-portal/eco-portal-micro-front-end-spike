import React, { useState } from "react";
import { Link } from "react-router-dom";

import "../index.css";
const img =
  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTOj0f1piPcvC6YkJIVydIlJyKrT7wgghbKkKg3hoMu&s";
const data = [
  {
    id: 1,
    name: "Fancis",
  },
  {
    id: 2,
    name: "Innocent",
  },
  {
    id: 3,
    name: "Matisse",
  },
  {
    id: 4,
    name: "Jacques",
  },
];

const List = () => {
  const [people, setPeople] = useState(data);
  const deleteName = (id) => setPeople(people.filter((el) => el.id !== id));
  return (
    <div className="container">
      <Link className="my-5" to="/test">
        GO TO TEST PAGE
      </Link>
      <div className="row">
        {people &&
          people.map((el) => (
            <div className="col-md-6" key={el.id}>
              <div className="card my-5 border-1">
                <div className="card-header">* {el.name} *</div>
                <div className="card-body">
                  <img src={img} className="card-img-top" alt="..." />
                </div>
                <div className="card-footer text-right">
                  <a
                    href="#"
                    onClick={() => deleteName(el.id)}
                    className="btn p-2 btn-primary"
                  >
                    DELETE
                  </a>
                </div>
              </div>
            </div>
          ))}
      </div>
    </div>
  );
};
export default List;

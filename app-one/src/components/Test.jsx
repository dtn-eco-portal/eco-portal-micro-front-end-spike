import React from "react";
import { Link } from "react-router-dom";

const Test = () => {
  return (
    <div className="container">
      <Link to="/"> GO TO LIST PAGE </Link>
      <h2 className="my-3"> ---- TEST PAGE ---- </h2>
    </div>
  );
};

export default Test;

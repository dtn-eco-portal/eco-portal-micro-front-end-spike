import React from "react";
import ReactDOM from "react-dom";
import AppOneApp from "./components/AppOneApp";

import "./index.css";
const logo =
  "https://cdn-fgnbn.nitrocdn.com/CExiFXHDXAeTGrlIkvRSSLnZISOqDumi/assets/static/optimized/rev-6e80265/wp-content/uploads/2019/03/dtn-logo-tag-492x108.png";
const App = () => {
  return (
    <>
      <nav className="navbar navbar-light bg-light">
        <a className="navbar-brand" href="#">
          <img src={logo} width="190" height="40" alt="" />
        </a>
      </nav>
      <h1 className="text-center my-5">WELCOME ON THE HOST APP</h1>
      <hr />
      <AppOneApp user="DTN-USER" />
    </>
  );
};

ReactDOM.render(<App />, document.getElementById("app"));

import { mount } from "appOne/AppOneApp";
import React, { useRef, useEffect } from "react";

export default ({ user }) => {
  const ref = useRef(null);

  useEffect(() => {
    mount(ref.current, user);
  });

  return <div ref={ref} />;
};
